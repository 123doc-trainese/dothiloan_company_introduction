# Dịch vụ xây dựng website giới thiệu công ty 

## Nội Dung:

- Xây dựng website giới thiệu công ty có chức năng phù hợp với yêu cầu khách hàng.
- Bao gồm : CSDL ( Cơ sở dữ liệu ), Trang quản trị và giao diện người dùng ( follow site tuvandongduong.vn  ).
- Áp dụng Công nghệ mới nhất cho website ( Front-end + Back-end ) và Responsive theme ( xây dựng cả giao diện cho cả mobile + tablet )
- Sitemap ( tổng quan ):
  - Trang chủ ( homepage ) : Slider banner + show list menu
  - Giới thiệu: Thông tin của công ty cần giới thiệu.
  - Sản phẩm - Dự án: list sản phẩm của công ty .
  - Tin tức -  sự kiện: chứa tin tức sự kiện mới về công ty.
