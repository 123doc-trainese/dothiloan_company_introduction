<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Product;
use App\Models\TypeProduct;
use App\Models\Category;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Post;

class PageController extends Controller
{
    public function introduction($slug='gioi-thieu'){
        $page = Page::whereSlug($slug)->first();
        $active = $slug;
        $seo_image = Voyager::image($page->image);
        return view('introduction', compact('page', 'active', 'seo_image'));
    }

    public function home() {
        $active = 'home';
        return view('home', compact('active'));
    }

    public function contact() {
        $active = 'contact';
        return view('contact', compact('active'));
    }

    // Sản phẩm dự án
    public function productType($type = null){
        $typeproducts = TypeProduct::all();
        if(!$type) {
            $active       = 'products.type';
            $products     = Product::paginate(9);
            return view('product.all', compact('active', 'products', 'typeproducts'));
        }

        $typeProduct  = TypeProduct::whereSlug($type)->firstOrFail();
        $products     = $typeProduct->products()->paginate(9);
        $active       = $type;
        return view('product.single-type', compact('active', 'typeProduct', 'typeproducts', 'products'));
    }

    public function productDetail($type, $slug){
        $product        = Product::whereSlug($slug)->firstOrFail();
        $typeProduct    = TypeProduct::whereSlug($type)->firstOrFail();
        $productOrthers = $typeProduct->products->except($product->id);
        $active  = $type;
        $seo_image = Voyager::image(json_decode($product->image)[0]);
        return view('product.show', compact('active', 'product', 'productOrthers', 'typeProduct', 'seo_image'));
    }

    //Tin tức sự kiện
    public function category($slug = null){
        $categories = Category::all();
        if(!$slug) {
            $active    = 'news.category';
            $posts     = Post::whereStatus('PUBLISHED')->paginate(9);
            return view('news.all', compact('active', 'posts', 'categories'));
        }

        $category  = Category::whereSlug($slug)->firstOrFail();
        $posts     = $category->products()->whereStatus('PUBLISHED')->paginate(9);
        $active    = $slug;
        return view('news.single-type', compact('active', 'category', 'categories', 'posts'));
    }

    public function postDetail($category, $slug){
        $post = Post::whereSlug($slug)->firstOrFail();
        $category  = Category::whereSlug($category)->firstOrFail();
        $postOthers = $category->posts->except($post->id);
        $active  = $category->slug;
        $seo_image = Voyager::image($post->image);
        return view('news.show', compact('active', 'post', 'postOthers', 'category', 'seo_image'));
    }
}
