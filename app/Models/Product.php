<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function typeProduct(){
        return $this->belongsTo(TypeProduct::class, 'type_id', 'id');
    }
}
