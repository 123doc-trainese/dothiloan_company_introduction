<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Models\Category as ModelsCategory;
use TCG\Voyager\Models\Post;

class Category extends ModelsCategory
{
    public function products(){
        return $this->hasMany(Post::class, 'category_id', 'id')->orderBy('created_at', 'desc');
    }
}
