<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeProduct extends Model
{
    protected $table = 'type_product';

    public function products(){
        return $this->hasMany(Product::class, 'type_id', 'id');
    }
}
