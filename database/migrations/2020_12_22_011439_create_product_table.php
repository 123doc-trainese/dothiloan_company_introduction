<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_id')->comment('id loại sản phẩm');
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('image')->default('products/product_default.png')->comment('list ảnh: mảng hoặc 1 ảnh');
//            $table->integer('price')->default(0)->comment('giá tiền');
//            $table->float('discount')->default(0)->comment('giảm giá, tính theo %');
//            $table->integer('amount')->default(1)->comment('số lượng còn trong kho');
            $table->string('slug')->unique();
            $table->timestamps();
            $table->index(['slug', 'created_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
