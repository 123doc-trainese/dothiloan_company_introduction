//js toggle menu item
$( ".header-list-item.parent" ).click(function() {
    $(this).toggleClass('active');
    $(this).find('.header-lists.child').slideToggle();
});
//js for nav bar toggle on tablet and mobile
$( ".header-container .nav-toggle-mobile" ).click(function() {
    $(this).toggleClass('active');
    $(this).parents('.header-container').find('ul.header-lists.parents').slideToggle();
});