<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('trang-chu', 'Site\PageController@home')->name('home');

Route::get('/', 'Site\PageController@home')->name('home');

Route::get('gioi-thieu/{page?}', 'Site\PageController@introduction')->name('introduction');

Route::get('san-pham/{type?}', 'Site\PageController@productType')->name('products.type');

Route::get('san-pham/{type}/{slug}', 'Site\PageController@productDetail')->name('products.show');

Route::get('tin-tuc-su-kien/{category?}', 'Site\PageController@category')->name('news.category');

Route::get('tin-tuc-su-kien/{category}/{slug}', 'Site\PageController@postDetail')->name('news.show');

Route::get('lien-he', 'Site\PageController@contact')->name('contact');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});