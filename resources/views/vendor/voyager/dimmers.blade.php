@php
$dimmerGroups = Voyager::dimmers();
$count = 0;
@endphp

@if (count($dimmerGroups))
<div class="clearfix container-fluid row">
@foreach($dimmerGroups as $dimmerGroup)
    @php
    $count += $dimmerGroup->count();
    $classes = [
        'col-xs-12',
        'col-sm-'.($count >= 2 ? '6' : '12'),
        'col-md-'.($count >= 3 ? '6' : ($count >= 2 ? '6' : '12')),
    ];
    $class = implode(' ', $classes);
    $prefix = "<div class='{$class}'>";
    $surfix = '</div>';
    @endphp
    @if ($dimmerGroup->any())
        {!! $prefix.$dimmerGroup->setSeparator($surfix.$prefix)->display().$surfix !!}
    @endif
@endforeach
</div>
@endif
