<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('/site.webmanifest')}}">
    <link rel="stylesheet" href="{{url('/OwlCarousel/dist/assets/owl.carousel.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- SEO -->
    <meta property="og:url"           content="{{ url()->current() }}" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="@yield('title')" />
    <meta property="og:description"   content="Công ty cổ phần tư vấn thiết kế T Design" />
    <meta property="og:image"         content="{{ $seo_image ?? '' }}" />
</head>

<body>
    <div class="header-container h-full w-1/6 hidden lg:block">
        <div class="logo-container"><a href="{{url('/')}}"><img class="mx-auto mt-4" src="{{url('/image/logo.png')}}"
                    alt=""></a></div>
        {{ menu('site', 'menu-site', ['active' => $active]) }}
        <div class="bottom-0 fixed p-10 text-center company-postion text-gray-600 text-sm w-1/6">
            <p class="mb-2">
            @if(!empty(\App\Models\Config::whereName('facebook')->first()->value))
                <a href="{{ \App\Models\Config::whereName('facebook')->first()->value }}">
                    <i class="fa fa-facebook-square fa-2x mx-1"></i>
                </a>
            @endif
            @if(!empty(\App\Models\Config::whereName('youtube')->first()->value))
                <a href="{{ \App\Models\Config::whereName('youtube')->first()->value }}">
                    <i class="fa fa-youtube-square fa-2x mx-1"></i>
                </a>
            @endif
            @if(!empty(\App\Models\Config::whereName('email')->first()->value))
                <a href="mailto:{{ \App\Models\Config::whereName('email')->first()->value }}">
                    <i class="fa fa-envelope fa-2x mx-1"></i>
                </a>
            @endif
            </p>
            <p class="mb-2">{{ strtoupper(\App\Models\Config::whereName('company_address')->first()->value) }}</p>
            <p>© COPYRIGHT {{date('Y')}}</p>
        </div>
        <div class="lg:hidden nav-toggle-mobile">
            <span class="op_line _1"></span>
            <span class="op_line _2"></span>
            <span class="op_line _3"></span>
        </div>
    </div>
    <div class="w-full lg:w-5/6 h-screen z-10 fixed top-0 right-0">
        <div class="h-full pt-12 lg:pt-0 overflow-scroll">
            @yield('content')
            @include('layouts.footer')
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0&appId=566919994139806&autoLogAppEvents=1" nonce="UTzYiOov"></script>
    <script src="{{url('/OwlCarousel/dist/owl.carousel.min.js')}}"></script>
    <script src="{{url('/js/animation.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
    $('.owl-1').owlCarousel({
        autoplay: true,
        items: 1,
        loop: true,
        margin: 0,
        stagePadding: 0,
        smartSpeed: 500,
    });
    $('.owl-3').owlCarousel({
        autoplay: true,
        items: 3,
        nav: true,
        loop: false,
        margin: 20,
        stagePadding: 0,
        smartSpeed: 500,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true,
                loop: true,
            },
            768: {
                items: 2,
                nav: true,
                loop: true,
            },
            1024: {
                items: 3,
                nav: true,
            }
        }
    });
    </script>
</body>

</html>
