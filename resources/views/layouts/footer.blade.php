<div class="bg-black bg-opacity-75 z-10 lg:hidden w-full z-10">
    <div class="text-center text-xs py-5 text-white w-full">
        <p class="mb-2">
        @if(!empty(\App\Models\Config::whereName('facebook')->first()->value))
                <a href="{{ \App\Models\Config::whereName('facebook')->first()->value }}">
                    <i class="fa fa-facebook-square fa-2x mx-1"></i>
                </a>
            @endif
            @if(!empty(\App\Models\Config::whereName('youtube')->first()->value))
                <a href="{{ \App\Models\Config::whereName('youtube')->first()->value }}">
                    <i class="fa fa-youtube-square fa-2x mx-1"></i>
                </a>
            @endif
            @if(!empty(\App\Models\Config::whereName('email')->first()->value))
                <a href="mailto:{{ \App\Models\Config::whereName('email')->first()->value }}">
                    <i class="fa fa-envelope fa-2x mx-1"></i>
                </a>
            @endif
        </p>
        <p class="mb-2">{{ strtoupper(\App\Models\Config::whereName('company_address')->first()->value) }}</p>
        <p>© COPYRIGHT {{date('Y')}}</p>
    </div>
</div>