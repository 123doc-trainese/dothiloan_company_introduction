@extends('layouts.master')

@section('title', $product->name)

@section('content')
<div class="w-screen lg:w-3/4 bg-white p-5 md:p-10">
    <div class="text-xl flex flex-wrap border-l-4 border-black pl-2">
        <a href="{{ route('products.type', $typeProduct->slug) }}" class="pr-1">{{ $typeProduct->name }}</a>
        >
        <p class="hover:text-black pl-2 cursor-auto">{{ $product->name }}</p></div>
    <hr class="my-3">

    @php $picturesItem = json_decode($product->image); @endphp
    <div class="md:grid md:grid-cols-5 md:gap-10">
        <div class="my-10 md:col-span-3">
            <div class="slider-home">
                <div class="owl-1 owl-carousel owl-theme">
                    @foreach($picturesItem as $item)
                        <div class="item">
                            <img src="{{ Voyager::image($item)}}" alt="">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="md:col-span-2 my-10">
            @php
                $price = 0;
                if ($product->discount > 0){
                    $price = round(($product->price - $product->price * $product->discount / 100), 3);
                }
            @endphp
            <p class="text-3xl mb-6 uppercase font-bold w-full">{{ $product->name }}</p>
            @if($price != 0)
                <div class="flex md:items-center">
                    <span class="text-xl md:text-2xl text-red-600 font-bold">{{ number_format($price, 0, ",", ".") }} đ</span>
                    <span class="text-sm md:text-lg line-through px-2">{{ number_format($product->price, 0, ",", ".") }} đ </span>
                    <span class="text-white bg-red-700 p-1 text-sm">{{ $product->discount }} % GIẢM</span>
                </div>
            @else
                <div class="flex md:items-center">
                    <span class="text-xl md:text-2xl text-red-600 font-bold">{{ number_format($product->price, 0, ",", ".") }} đ </span>
                </div>
            @endif

            <p class="mt-6"><strong>Số lượng: </strong>{{ $product->amount }}</p>
        </div>
    </div>

    <div class="mb-10">
        <strong class=" md:text-xl">Mô tả sản phẩm:</strong>
        {!! $product->description !!}
    </div>

    <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>

    <div>
        <h3 class="text-2xl mt-10">BÀI VIẾT KHÁC</h3>
        <hr class="mb-3">
        <div class="owl-3 owl-carousel owl-theme w-full">
            @foreach($productOrthers as $productOrther)
            <div class="item h-64 ">
                <a href="{{ route('products.show', ['type'=> $typeProduct->slug,'slug'=> $productOrther->slug]) }}">
                    @php $pictures = json_decode($productOrther->image); @endphp
                    <span class="product-image">
                        @if (isset($pictures[0]))
                        <img src="{{ Voyager::image($pictures[0]) }}" alt="" class="img h-full">
                        @else
                        <img src="{{ Voyager::image('product/default.jpg') }}" alt="" class="img h-full">
                        @endif
                    </span>
                </a>
                <div class="product-detail">
                    <a href="{{ route('products.show', ['type'=> $typeProduct->slug,'slug'=> $productOrther->slug]) }}"
                        class="product-name">{{ $productOrther->name }}</a>
                    <a href="{{ route('products.type', $typeProduct->slug) }}"
                        class="product-type">{{ $typeProduct->name }}</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
