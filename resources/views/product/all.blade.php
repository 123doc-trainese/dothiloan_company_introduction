@extends('layouts.master')

@section('title', 'Sản phẩm - Dự án')

@section('content')
<div class="bg-white p-5 md:p-10">
    <h2 class="text-3xl uppercase">Sản phẩm - Dự án</h2>
    <hr class="mb-3">
    <ul class="product-type-list">
        <li class="active"><span>Tất cả</span></li>
        @foreach($typeproducts as $typeproduct)
        <li><a href="{{ route('products.type', $typeproduct->slug) }}">{{ $typeproduct->name }}</a></li>
        @endforeach
    </ul>
    <div class="list-products">
        @forelse($products as $product)
        <div class="p-2 h-64 inline-block relative w-full md:w-1/2-fix lg:w-1/3-fix">
            <a href="{{ route('products.show', ['type'=> $product->typeProduct->slug,'slug'=> $product->slug]) }}">
                @php $pictures = json_decode($product->image); @endphp
                <span class="product-image">
                    @if (isset($pictures[0]))
                    <img src="{{ Voyager::image($pictures[0]) }}" alt="" class="img h-full">
                    @else
                    <img src="{{ Voyager::image('product/default.jpg') }}" alt="" class="img h-full">
                    @endif
                </span>
            </a>
            <div class="product-detail">
                <a href="{{ route('products.show', ['type'=> $product->typeProduct->slug,'slug'=> $product->slug]) }}"
                    class="product-name">{{ $product->name }}</a>
                <a href="{{ route('products.type', $product->typeProduct->slug) }}"
                    class="product-type">{{ $product->typeProduct->name }}</a>
            </div>
        </div>
        @empty
        <div class="absolute text-center top-1/2 w-full">
            Không có nội dung
        </div>
        @endforelse
    </div>
    <div class="mt-4 text-center md:text-left">
    {{ $products->onEachSide(2)->links() }}
    </div>
</div>
@stop
