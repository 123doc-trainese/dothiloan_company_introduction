@extends('layouts.master')

@section('title', 'Liên hệ')

@section('content')
<div class="lg:w-3/4 bg-white p-5 md:p-10">
    <h2 class="text-3xl uppercase">LIÊN HỆ</h2>
    <hr class="mb-10">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3725.4578044950617!2d105.84789141470837!3d20.974278195012314!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac464d39eaff%3A0xe87e2ddff0ead574!2zU-G7kCA5QSwgMTUyIE5nw7UgMTQxIEdpw6FwIE5o4buLLCBHacOhcCBOaOG7iywgVGjhu4tuaCBMaeG7h3QsIEhvw6BuZyBNYWksIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1612498986181!5m2!1svi!2s"
        frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="w-full mb-4" height="500"></iframe>
    <div class="md:flex">
        <div class="md:w-2/3 w-full">
            <h3 class="text-2xl mb-2">CÔNG TY CỔ PHẦN TƯ VẤN THIẾT KẾ T DESIGN</h3>
            <p class="mb-4"><i class="fa fa-map-marker fa-lg mr-1"></i>
                {{ strtoupper(\App\Models\Config::whereName('company_address')->first()->value) }}</p>
            <p class="mb-2 text-gray-700">
                <a href="callto:{{ \App\Models\Config::whereName('phone')->first()->value }}">
                    <i class="fa fa-phone-square fa-lg mr-1"></i>
                    {{ \App\Models\Config::whereName('phone')->first()->value }}
                </a>
                </br>
                <a href="mailto:{{ \App\Models\Config::whereName('email')->first()->value }}">
                    <i class="fa fa-envelope fa-lg mr-1"></i>
                    {{ \App\Models\Config::whereName('email')->first()->value }}
                </a>
            </p>
        </div>
        <div class="md:w-1/3 md:pl-10 w-full">
            <h3 class="text-2xl mb-2">LIÊN KẾT SOCIAL</h3>
            <p class="mb-4">
            @if(!empty(\App\Models\Config::whereName('facebook')->first()->value))
                <a href="{{ \App\Models\Config::whereName('facebook')->first()->value }}">
                    <i class="fa fa-facebook-square fa-4x mx-1" style="color: #1877f2;"></i>
                </a>
            @endif
            @if(!empty(\App\Models\Config::whereName('youtube')->first()->value))
                <a href="{{ \App\Models\Config::whereName('youtube')->first()->value }}">
                    <i class="fa fa-youtube-square fa-4x mx-1" style="color: #f34423;"></i>
                </a>
            @endif
            </p>
        </div>
    </div>
</div>
@endsection