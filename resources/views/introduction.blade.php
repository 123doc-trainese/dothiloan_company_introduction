@extends('layouts.master')

@section('title', $page->title)

@section('content')
<div class="lg:w-3/4 bg-white p-5 md:p-10">
    <h2 class="text-3xl uppercase">{{ $page->title }}</h2>
    <hr class="mb-3">
    {!! $page->body !!}
    
    <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>

</div>
@endsection