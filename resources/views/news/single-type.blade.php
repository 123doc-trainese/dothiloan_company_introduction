@extends('layouts.master')

@section('title', $category->name)

@section('content')
<div class="bg-white p-5 md:p-10">
    <h2 class="text-3xl uppercase">Tin tức - Sự kiện</h2>
    <hr class="mb-3">
    <ul class="product-type-list">
        <li><a href="{{ route('news.category') }}">Tất cả</a></li>
        @foreach($categories as $cate)
        @if($cate->slug == $active)
        <li class="active"><span>{{ $cate->name }}</span></li>
        @else
        <li><a href="{{ route('news.category', $cate->slug) }}">{{ $cate->name }}</a></li>
        @endif
        @endforeach
    </ul>
    <div class="list-products">
        <ul class="product-items">
            @forelse($posts as $post)
            <li class="p-2 h-64 inline-block relative w-full md:w-1/2-fix lg:w-1/3-fix">
                <a href="{{ route('news.show', ['category'=> $category->slug,'slug'=> $post->slug]) }}">
                    <span class="product-image">
                        @if ($post->image)
                        <img src="{{ Voyager::image($post->image) }}" alt="" class="img h-full">
                        @else
                        <img src="{{ Voyager::image('product/default.jpg') }}" alt="" class="img h-full">
                        @endif
                    </span>
                </a>
                <div class="product-detail">
                    <a href="{{ route('news.show', ['category'=> $category->slug,'slug'=> $post->slug]) }}"
                        class="product-name">{{ \Str::limit($post->title, 66, '...') }}</a>
                    <a href="{{ route('news.category', $post->category->slug) }}"
                        class="product-type">{{ $post->category->name }}</a>
                </div>
            </li>
            @empty
            <div class="absolute text-center top-1/2 w-full">
                Không có nội dung
            </div>
            @endforelse
        </ul>
    </div>
    <div class="mt-4 text-center md:text-left">
    {{ $posts->onEachSide(2)->links() }}
    </div>
</div>
@stop