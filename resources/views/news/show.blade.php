@extends('layouts.master')

@section('title', $post->title)

@section('content')
<div class="w-screen lg:w-3/4 bg-white p-5 md:p-10">
    <h2 class="text-3xl uppercase">{{ $post->title }}</h2>
    <hr class="mb-3">
    {!! $post->body !!}

    <div class="fb-share-button" data-href="{{ url()->current() }}" data-layout="button_count" data-size="large"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Chia sẻ</a></div>

    <div>
        <h3 class="text-2xl mt-10">BÀI VIẾT KHÁC</h3>
        <hr class="mb-3">
        <div class="owl-3 owl-carousel owl-theme w-full">
            @foreach($postOthers as $post)
            <div class="item h-64 ">
                <a href="{{ route('news.show', ['category'=> $category->slug,'slug'=> $post->slug]) }}">
                    <span class="product-image">
                        @if ($post->image)
                        <img src="{{ Voyager::image($post->image) }}" alt="" class="img h-full">
                        @else
                        <img src="{{ Voyager::image('product/default.jpg') }}" alt="" class="img h-full">
                        @endif
                    </span>
                </a>
                <div class="product-detail">
                    <a href="{{ route('news.show', ['category'=> $category->slug,'slug'=> $post->slug]) }}"
                        class="product-name">{{ \Str::limit($post->title, 66, '...') }}</a>
                    <a href="{{ route('news.category', $post->category->slug) }}"
                        class="product-type">{{ $post->category->name }}</a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

@endsection