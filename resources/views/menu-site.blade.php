<ul class="header-lists parents text-gray-500">
    @foreach($items as $menu_item)
    <?php
        $parameters = $menu_item->getParametersAttribute();
    ?>
        @if(!is_null($parameters))
        <?php
            $dataType = \Voyager::model('DataType')->where('name', '=', $parameters->model)->first();
            $subMenu = app($dataType->model_name)->all();
            $display = false;
            foreach($subMenu as $menu) {
                if($menu->slug == $options->active) {
                    $display = true;
                    break;
                }
            }
        ?>
        <li class="header-list-item parent">
            <a class="uppercase {{$options->active == $menu_item->route ? 'text-white' : ''}}" href="{{ $menu_item->link() }}"><span>{{ $menu_item->title }}</span></a>
            <ul class="header-lists child {{ $display ? 'block' : '' }}">
            @foreach($subMenu as $menu)
                <li class="header-list-item child-item capitalize {{ $menu->slug == $options->active ? 'text-white' : ''}}"><a href="{{ url(route($menu_item->route, $menu->slug)) }}"><span>{{$menu->title ?? $menu->name}}</span></a></li>
            @endforeach
            </ul>
        </li>
        @else
        <li class="header-list-item uppercase {{$options->active == $menu_item->route ? 'text-white' : ''}}"><a href="{{ url($menu_item->link()) }}"><span>{{ $menu_item->title }}</span></a></li>
        @endif
    @endforeach
</ul>
