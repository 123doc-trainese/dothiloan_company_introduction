@extends('layouts.master')

@section('title', 'CÔNG TY CỔ PHẦN TƯ VẤN THIẾT KẾ T-DESIGN')

@section('content')
<?php
    $slides = \App\Models\Slide::all();
?>
<div class="content-container">
    <div class="slider-home">
        <div class="owl-1 owl-carousel owl-theme">
            @foreach($slides as $slide)
            <div class="item">
                <img src="{{ Voyager::image($slide->image)}}" alt="">
                <?php
                    $contents = explode('|', $slide->title);
                ?>
                <div
                    class="uppercase bg-black bg-opacity-75 border-2 border-dashed fixed md:left-1/4 px-5 md:px-10 ml-5 py-3 top-2/5 z-10">
                    <h1 class="text-2xl md:text-4xl text-white">{{ $contents[0] }}</h1>
                    @foreach($contents as $key=>$content)
                    @if($key > 0)
                    <p class="md:text-xl text-white">{{ $content }}</p>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
