<?php

return [
    'category'               => 'Thể loại bài viết',
    'category_link_text'     => 'Tất cả thể loại bài viết',
    'category_text'          => 'Bạn có :count :string trong cơ sở dữ liệu. Bấm vào nút bên dưới để xem tất cả thế loại.',
    'type_product'           => 'Loại Sản phẩm - Dự án',
    'type_product_link_text' => 'Tất cả loại Sản phẩm - Dự án',
    'type_product_text'      => 'Bạn có :count :string trong cơ sở dữ liệu. Bấm vào nút bên dưới để xem tất cả loại SP-DA.',
    'product'                => 'Sản phẩm - Dự án',
    'product_link_text'      => 'Tất cả Sản phẩm - Dự án',
    'product_text'           => 'Bạn có :count :string trong cơ sở dữ liệu. Bấm vào nút bên dưới để xem tất cả Sản phẩm - Dự án.',
];