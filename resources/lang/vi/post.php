<?php

return [
    'additional_fields'=> 'Các trường bổ sung',
    'category'         => 'Thể loại',
    'content'          => 'Nội dung bài viết',
    'details'          => 'Chi tiết bài viết',
    'excerpt'          => 'Tóm tắt <small> Mô tả nhỏ về bài đăng này </ small>',
    'image'            => 'Hình ảnh bài viết',
    'meta_description' => 'Mô tả META',
    'meta_keywords'    => 'Từ khóa META',
    'new'              => 'Đăng bài',
    'seo_content'      => 'Nội dung SEO',
    'seo_title'        => 'Tiêu đề SEO',
    'slug'             => 'URL slug',
    'status'           => 'Trạng thái bài viết',
    'status_draft'     => 'Nháp',
    'status_pending'   => 'Đang chỉnh sửa',
    'status_published' => 'Phát hành',
    'title'            => 'Tiêu đề bài viết',
    'title_sub'        => 'Tiêu đề cho bài viết của bạn',
    'update'           => 'Cập nhật bài viết',
];
