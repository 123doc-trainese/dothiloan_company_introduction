module.exports = {
  purge: [
    './resources/**/*.blade.php',
     './resources/**/*.js',
     './resources/**/*.vue',
  ],
  important: true,
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      width: {
        '1/2-fix': 'calc(50% - 3px)',
        '1/3-fix': 'calc(33.333333% - 3px)',
      }
    },
    inset: {
      '0': 0,
      auto: 'auto',
      '1/2': '50%',
      '1/5': '20%',
      '1/4': '25%',
      '2/5': '40%',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
  ],
}
