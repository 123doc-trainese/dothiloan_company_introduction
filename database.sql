-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th9 23, 2022 lúc 06:44 AM
-- Phiên bản máy phục vụ: 10.5.12-MariaDB-cll-lve
-- Phiên bản PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `u953009565_tdsign`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Tin tức nổi bật', 'tin-tuc-noi-bat', '2020-12-21 02:36:01', '2020-12-22 21:19:43'),
(2, NULL, 1, 'Sự kiện trong nước', 'su-kien-trong-nuoc', '2020-12-21 02:36:01', '2020-12-22 21:19:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `configs`
--

CREATE TABLE `configs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `configs`
--

INSERT INTO `configs` (`id`, `name`, `display_name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'company_address', 'Địa chỉ', 'SỐ 9A, NGÕ 141/152 ĐƯỜNG GIÁP NHỊ, PHƯỜNG THỊNH LIỆT, QUẬN HOÀNG MAI, HÀ NỘI, VIỆT NAM', '2021-02-04 19:55:19', '2021-02-04 19:55:19'),
(2, 'phone', 'Số điện thoại', '0965910466', '2021-02-04 19:56:07', '2021-02-04 19:56:07'),
(6, 'email', 'Email', 'tuuvanthieketdesign@gmail.com', '2021-02-04 19:59:53', '2021-02-04 19:59:53'),
(7, 'facebook', 'Facebook', 'https://www.facebook.com/groups/ChoXacDienThoaiToanQuoc', '2021-02-04 20:00:14', '2021-02-04 20:00:14'),
(8, 'youtube', 'Youtube', 'https://www.youtube.com/channel/UC5ezaYrzZpyItPSRG27MLpg', '2021-02-04 20:00:47', '2021-02-04 22:29:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:categories,slug\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '{}', 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '{}', 2),
(31, 5, 'category_id', 'text', 'Category', 0, 0, 1, 1, 1, 0, '{}', 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 0, 0, 1, 1, 1, 1, '{}', 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '{}', 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"400\",\"height\":\"300\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 0, 0, 0, 0, 0, 0, '{}', 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 0, 0, 0, 0, 0, 0, '{}', 10),
(39, 5, 'status', 'select_dropdown', 'Trạng thái', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"C\\u00f4ng khai\",\"DRAFT\":\"Nh\\u00e1p\"}}', 13),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 14),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 15),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 0, 0, 0, 0, 0, '{}', 16),
(43, 5, 'featured', 'checkbox', 'Đặc sắc', 1, 1, 1, 1, 1, 1, '{\"on\":\"C\\u00f3\",\"off\":\"Kh\\u00f4ng\",\"checked\":false}', 17),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '{}', 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 0, 0, 0, 0, 0, 0, '{}', 5),
(48, 6, 'body', 'rich_text_box', 'Nội dung', 0, 0, 1, 1, 1, 1, '{}', 10),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:pages,slug\"},\"readonly\":true}', 4),
(50, 6, 'meta_description', 'text', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 6),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 7),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 8),
(53, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 11),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 12),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '{\"quality\":\"70%\",\"upsize\":true}', 9),
(56, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 7, 'type_id', 'text', 'Type Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(58, 7, 'name', 'text', 'Tên SP-DA(*)', 1, 1, 1, 1, 1, 1, '{}', 3),
(59, 7, 'description', 'rich_text_box', 'Mô tả', 0, 1, 1, 1, 1, 1, '{}', 10),
(60, 7, 'image', 'multiple_images', 'Ảnh', 1, 1, 1, 1, 1, 1, '{\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"300\"}}]}', 9),
(64, 7, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:product,slug\"},\"readonly\":true}', 4),
(65, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{\"format\":\"%d\\/%m\\/%Y l\\u00fac %H:%M\"}', 11),
(66, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{\"format\":\"%d\\/%m\\/%Y l\\u00fac %H:%M\"}', 12),
(67, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(68, 8, 'name', 'text', 'Tên', 1, 1, 1, 1, 1, 1, '{}', 2),
(69, 8, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:type_product,slug\"}}', 3),
(70, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{\"format\":\"%Y\\/%m\\/%d l\\u00fac %H:%M\"}', 4),
(71, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{\"format\":\"%Y\\/%m\\/%d l\\u00fac %H:%M\"}', 5),
(72, 5, 'post_belongsto_user_relationship', 'relationship', 'Người viết', 0, 1, 0, 0, 0, 0, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"author_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 12),
(73, 7, 'product_belongsto_type_product_relationship', 'relationship', 'Loại SP-DA', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\TypeProduct\",\"table\":\"type_product\",\"type\":\"belongsTo\",\"column\":\"type_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(74, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(75, 9, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(76, 9, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{\"quality\":\"70%\",\"upsize\":true}', 3),
(77, 9, 'link', 'text', 'Link', 0, 0, 0, 0, 0, 0, '{}', 4),
(78, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 5),
(79, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(80, 5, 'post_belongsto_category_relationship', 'relationship', 'Thể loại', 0, 1, 0, 0, 0, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Category\",\"table\":\"categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(87, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(88, 11, 'name', 'text', 'Name', 1, 0, 0, 0, 0, 0, '{}', 2),
(89, 11, 'display_name', 'text', 'Thông tin', 1, 1, 1, 0, 0, 0, '{}', 3),
(90, 11, 'value', 'text', 'Giá trị', 0, 1, 1, 1, 1, 0, '{}', 4),
(91, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 5),
(92, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-12-21 02:36:01', '2021-02-04 21:58:01'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-12-21 02:36:02', '2021-02-04 21:58:54'),
(6, 'pages', 'pages', 'Trang', 'Trang', 'voyager-file-text', 'App\\Models\\Page', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-12-21 02:36:02', '2021-02-04 21:58:14'),
(7, 'product', 'product', 'Sản phẩm - Dự án', 'Sản phẩm - Dự án', 'voyager-bookmark', 'App\\Models\\Product', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"updated_at\",\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-12-22 03:28:23', '2021-02-04 21:57:54'),
(8, 'type_product', 'type-product', 'Loại Sản phẩm - Dự án', 'Loại Sản phẩm - Dự án', 'voyager-list', 'App\\Models\\TypeProduct', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"created_at\",\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":\"name\",\"scope\":null}', '2020-12-22 03:42:04', '2021-02-04 22:03:28'),
(9, 'slide', 'slide', 'Slide', 'Slides', NULL, 'App\\Models\\Slide', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-01-01 10:33:58', '2021-02-04 21:59:20'),
(11, 'configs', 'configs', 'Cài đặt', 'Cài đặt', 'voyager-settings', 'App\\Models\\Config', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-02-05 06:25:18', '2021-02-05 06:25:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(2, 'site', '2020-12-22 03:06:17', '2020-12-22 03:06:17');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-12-21 02:36:01', '2020-12-21 02:36:01', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 10, '2020-12-21 02:36:01', '2021-01-01 10:35:14', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 9, '2020-12-21 02:36:01', '2021-01-01 10:35:14', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 8, '2020-12-21 02:36:01', '2021-01-01 10:35:21', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 12, '2020-12-21 02:36:01', '2021-02-04 19:53:21', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-12-21 02:36:01', '2020-12-22 18:45:36', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-12-21 02:36:01', '2020-12-22 18:45:36', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-12-21 02:36:01', '2020-12-22 18:45:36', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-12-21 02:36:01', '2020-12-22 18:45:36', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 13, '2020-12-21 02:36:01', '2021-02-04 19:53:22', 'voyager.settings.index', NULL),
(11, 1, 'Thể loại bài viết', '', '_self', 'voyager-categories', '#000000', NULL, 4, '2020-12-21 02:36:01', '2020-12-22 18:46:39', 'voyager.categories.index', 'null'),
(12, 1, 'Bài viết', '', '_self', 'voyager-news', '#000000', NULL, 5, '2020-12-21 02:36:02', '2020-12-22 18:46:39', 'voyager.posts.index', 'null'),
(13, 1, 'Trang', '', '_self', 'voyager-file-text', '#000000', NULL, 6, '2020-12-21 02:36:02', '2021-01-01 10:35:19', 'voyager.pages.index', 'null'),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-12-21 02:36:02', '2020-12-22 18:45:36', 'voyager.hooks', NULL),
(15, 2, 'Trang chủ', '', '_self', 'voyager-home', '#ffffff', NULL, 1, '2020-12-22 03:07:30', '2021-01-01 02:22:04', 'home', NULL),
(16, 2, 'Giới thiệu', '', '_self', 'voyager-info-circled', '#ffffff', NULL, 4, '2020-12-22 03:09:10', '2021-01-05 20:56:42', 'introduction', '{\"model\":\"pages\"}'),
(17, 2, 'Sản phẩm - Dự án', '', '_self', 'voyager-bookmark', '#ffffff', NULL, 2, '2020-12-22 03:13:34', '2021-01-05 19:21:15', 'products.type', '{\"model\":\"type_product\"}'),
(18, 2, 'Tin tức -  sự kiện', '', '_self', 'voyager-news', '#ffffff', NULL, 3, '2020-12-22 03:16:07', '2021-01-07 15:12:41', 'news.category', '{\"model\":\"categories\"}'),
(19, 1, 'Sản phẩm - Dự án', '', '_self', 'voyager-bookmark', NULL, NULL, 3, '2020-12-22 03:28:23', '2020-12-22 18:45:41', 'voyager.product.index', NULL),
(20, 1, 'Loại Sản phẩm - Dự án', '', '_self', 'voyager-list', '#000000', NULL, 2, '2020-12-22 03:42:04', '2020-12-22 18:45:41', 'voyager.type-product.index', 'null'),
(25, 1, 'Slides', '', '_self', 'voyager-images', '#000000', NULL, 7, '2021-01-01 10:33:58', '2021-01-01 10:36:00', 'voyager.slide.index', 'null'),
(28, 1, 'Cài đặt', '', '_self', 'voyager-settings', NULL, NULL, 11, '2021-02-05 06:25:18', '2021-02-05 06:25:33', 'voyager.configs.index', NULL),
(29, 2, 'Liên hệ', '', '_self', NULL, '#000000', NULL, 14, '2021-02-05 06:27:26', '2021-02-05 06:27:26', 'contact', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2019_08_19_000000_create_failed_jobs_table', 1),
(24, '2016_01_01_000000_create_pages_table', 2),
(25, '2016_01_01_000000_create_posts_table', 2),
(26, '2016_02_15_204651_create_categories_table', 2),
(27, '2017_04_11_000000_alter_post_nullable_fields_table', 2),
(28, '2020_12_22_011411_create_type_product_table', 3),
(29, '2020_12_22_011439_create_product_table', 3),
(30, '2020_12_22_011518_create_slide_table', 3),
(31, '2020_12_23_104532_change_type_description_product_table', 4),
(32, '2021_02_05_024425_create_configs_table', 5);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Giới thiệu', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<div class=\"titBox\" style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-bottom: 25px; color: #222222; font-size: 15px;\">\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: center;\"><strong style=\"box-sizing: border-box;\">C&Ocirc;NG TY CỔ PHẦN TƯ VẤN THIẾT KẾ T DESIGN</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">Được th&agrave;nh lập năm 2010, C&ocirc;ng ty Cổ phần tư vấn thiết kế T Design (TDS) l&agrave; một trong những c&ocirc;ng ty c&oacute; bề d&agrave;y kinh nghiệm về cung cấp đủ c&aacute;c dịch vụ của C&ocirc;ng ty tư vấn thiết kế bao gồm tư vấn thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y, tư vấn thiết kế hệ thống điện, điện nhẹ, điều h&ograve;a kh&ocirc;ng kh&iacute;, cấp tho&aacute;t nước&hellip;với c&aacute;c chủ đầu tư h&agrave;ng đầu tại Việt Nam.</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">Với tinh thần &ldquo;lu&ocirc;n thay đổi để th&agrave;nh c&ocirc;ng&rdquo;, TDS lu&ocirc;n cam kết mang đến kh&aacute;ch h&agrave;ng dịch vụ tư vấn cao cấp với những s&aacute;ng tạo gi&aacute; trị dựa tr&ecirc;n nền tảng kỹ thuật ti&ecirc;n tiến v&agrave; sự thấu hiểu kh&aacute;ch h&agrave;ng s&acirc;u sắc. Suốt những năm qua, với chất lượng sản phẩm c&ugrave;ng tinh thần nhiệt huyết, TDS khẳng định m&igrave;nh l&agrave; người bạn đồng h&agrave;nh đ&aacute;ng tin cậy của c&aacute;c chủ đầu tư lớn. Nhờ đ&oacute;, TDS lu&ocirc;n được c&aacute;c đối t&aacute;c tin tưởng lựa chọn để thực hiện những dự &aacute;n quy m&ocirc; lớn tại Việt Nam hiện nay.</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">TDS lu&ocirc;n x&aacute;c định con người ch&iacute;nh l&agrave; yếu tố then chốt cho sự ph&aacute;t triển vững mạnh của doanh nghiệp. Tinh thần học hỏi để n&acirc;ng cao tr&igrave;nh độ chuy&ecirc;n m&ocirc;n lu&ocirc;n được khuyến kh&iacute;ch. Ban l&atilde;nh đạo lu&ocirc;n tạo điều kiện thuận lợi cho đội ngũ nh&acirc;n vi&ecirc;n tham gia c&aacute;c kh&oacute;a đ&agrave;o tạo nội bộ v&agrave; b&ecirc;n ngo&agrave;i để bồi dưỡng ph&aacute;t triển kỹ năng l&agrave;m việc, trau dồi kiến thức, ph&aacute;t huy hết khả năng s&aacute;ng tạo trong c&ocirc;ng việc.</p>\r\n<hr style=\"box-sizing: content-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; height: 0px; margin-top: 21px; margin-bottom: 21px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #dddddd; color: #222222; font-size: 15px;\" />\r\n<p dir=\"ltr\" style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">TẦM NH&Igrave;N</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Trở th&agrave;nh c&ocirc;ng ty</span>&nbsp;h&agrave;ng đầu Việt Nam&nbsp;<span style=\"box-sizing: border-box;\">trong c&aacute;c lĩnh vực tư vấn thiết kế PCCC-Cơ điện.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">SỨ MỆNH</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Khẳng định chất lượng sản phẩm, dịch vụ, mang tới đối t&aacute;c những trải nghiệm chuy&ecirc;n nghiệp, văn minh v&agrave; đẳng cấp.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">GI&Aacute; TRỊ CỐT L&Otilde;I</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Chất lượng, tiến độ l&agrave; trọng t&acirc;m</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">TRIẾT L&Yacute; KINH DOANH</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Gi&aacute; trị của TDS lu&ocirc;n đi k&egrave;m những cam kết về sự chuy&ecirc;n nghiệp v&agrave; tinh thần tr&aacute;ch nhiệm, đề cao uy t&iacute;n v&agrave; danh dự trong việc n&acirc;ng cao chất lượng dịch vụ đến&nbsp;</span><span style=\"box-sizing: border-box;\">đối t&aacute;c</span><strong style=\"box-sizing: border-box;\">.</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><strong style=\"box-sizing: border-box;\">LỊCH SỬ PH&Aacute;T TRIỂN</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">C&ocirc;ng ty Cổ phần tư vấn thiết kế T Design được th&agrave;nh lập v&agrave;o năm 2011. Đến nay, T Design đ&atilde; trở th&agrave;nh một doanh nghiệp lớn mạnh với hơn 40 nh&acirc;n&nbsp; vi&ecirc;n.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><strong style=\"box-sizing: border-box;\">TDS L&Agrave;M G&Igrave;?</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">TDS cung cấp đầy đủ c&aacute;c dịch vụ của C&ocirc;ng ty tư vấn thiết kế hệ thống cơ điện, ph&ograve;ng ch&aacute;y chữa ch&aacute;y bao gồm thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y, thiết kế điện, cấp tho&aacute;t nước, điều h&ograve;a kh&ocirc;ng kh&iacute;, điện nhẹ</span><span style=\"box-sizing: border-box;\">. Ch&uacute;ng t&ocirc;i lu&ocirc;n tự h&agrave;o khi c&oacute; nhiều năm kinh nghiệm tham gia c&aacute;c dự &aacute;n lớn v&agrave; c&oacute; c&ocirc;ng năng phức tạp như&nbsp;</span><span style=\"box-sizing: border-box;\">t&ograve;a nh&agrave; THC Tower, Tropical village B&igrave;nh Minh &ndash; Hội An, Nh&agrave; xưởng sản xuất phụ t&ugrave;ng, sửa chữa, bảo dưỡng &ocirc; t&ocirc; Gia L&acirc;m , Nh&agrave; m&aacute;y phở tại KCN Ti&ecirc;n Sơn, Bắc Ninh, C.ty CP y khoa Th&aacute;i Thịnh, CT TNHH DLND LBNĐ Hội An&nbsp;</span><span style=\"box-sizing: border-box;\">&nbsp;&hellip; c&ugrave;ng với c&aacute;c chủ đầu tư uy t&iacute;n, c&oacute; y&ecirc;u cầu cao về chất lượng sản phẩm</span><span style=\"box-sizing: border-box;\">. Ch&uacute;ng t&ocirc;i tin tưởng rằng với nỗ lực v&agrave; sự tin tưởng của qu&yacute; kh&aacute;ch h&agrave;ng, C&ocirc;ng ty sẽ lu&ocirc;n khẳng định được uy t&iacute;n v&agrave; vững bước đi đến th&agrave;nh c&ocirc;ng. TDS cam kết mang đến qu&yacute; kh&aacute;ch h&agrave;ng dịch vụ tốt nhất ở mỗi dự &aacute;n m&agrave; ch&uacute;ng t&ocirc;i thực hiện.</span></p>\r\n<ul style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-top: 0px; margin-bottom: 20px; line-height: 1.4; color: #646464; clear: both; padding-left: 25px; font-size: 15px;\">\r\n<li style=\"box-sizing: border-box; font-weight: inherit; list-style: square;\"><strong style=\"box-sizing: border-box;\">Tư vấn thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y</strong></li>\r\n</ul>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">&nbsp;</p>\r\n<ul style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-top: 0px; margin-bottom: 20px; line-height: 1.4; color: #646464; clear: both; padding-left: 25px; font-size: 15px;\">\r\n<li style=\"box-sizing: border-box; font-weight: inherit; list-style: square;\"><strong style=\"box-sizing: border-box;\">Tư vấn thiết kế cơ điện</strong></li>\r\n</ul>\r\n</div>', 'pages/January2021/D1m5cL4p7v4K6g0KvUKu.png', 'gioi-thieu', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2020-12-21 02:36:02', '2021-03-25 15:40:02'),
(2, 1, 'Giới thiệu chung', NULL, '<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">&nbsp;</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: center;\"><strong style=\"box-sizing: border-box;\">C&Ocirc;NG TY CỔ PHẦN TƯ VẤN THIẾT KẾ T DESIGN</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">Được th&agrave;nh lập năm 2010, C&ocirc;ng ty Cổ phần tư vấn thiết kế T Design (TDS) l&agrave; một trong những c&ocirc;ng ty c&oacute; bề d&agrave;y kinh nghiệm về cung cấp đủ c&aacute;c dịch vụ của C&ocirc;ng ty tư vấn thiết kế bao gồm tư vấn thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y, tư vấn thiết kế hệ thống điện, điện nhẹ, điều h&ograve;a kh&ocirc;ng kh&iacute;, cấp tho&aacute;t nước&hellip;với c&aacute;c chủ đầu tư h&agrave;ng đầu tại Việt Nam.</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">Với tinh thần &ldquo;lu&ocirc;n thay đổi để th&agrave;nh c&ocirc;ng&rdquo;, TDS lu&ocirc;n cam kết mang đến kh&aacute;ch h&agrave;ng dịch vụ tư vấn cao cấp với những s&aacute;ng tạo gi&aacute; trị dựa tr&ecirc;n nền tảng kỹ thuật ti&ecirc;n tiến v&agrave; sự thấu hiểu kh&aacute;ch h&agrave;ng s&acirc;u sắc. Suốt những năm qua, với chất lượng sản phẩm c&ugrave;ng tinh thần nhiệt huyết, TDS khẳng định m&igrave;nh l&agrave; người bạn đồng h&agrave;nh đ&aacute;ng tin cậy của c&aacute;c chủ đầu tư lớn. Nhờ đ&oacute;, TDS lu&ocirc;n được c&aacute;c đối t&aacute;c tin tưởng lựa chọn để thực hiện những dự &aacute;n quy m&ocirc; lớn tại Việt Nam hiện nay.</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">TDS lu&ocirc;n x&aacute;c định con người ch&iacute;nh l&agrave; yếu tố then chốt cho sự ph&aacute;t triển vững mạnh của doanh nghiệp. Tinh thần học hỏi để n&acirc;ng cao tr&igrave;nh độ chuy&ecirc;n m&ocirc;n lu&ocirc;n được khuyến kh&iacute;ch. Ban l&atilde;nh đạo lu&ocirc;n tạo điều kiện thuận lợi cho đội ngũ nh&acirc;n vi&ecirc;n tham gia c&aacute;c kh&oacute;a đ&agrave;o tạo nội bộ v&agrave; b&ecirc;n ngo&agrave;i để bồi dưỡng ph&aacute;t triển kỹ năng l&agrave;m việc, trau dồi kiến thức, ph&aacute;t huy hết khả năng s&aacute;ng tạo trong c&ocirc;ng việc.</p>\r\n<hr style=\"box-sizing: content-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; height: 0px; margin-top: 21px; margin-bottom: 21px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #dddddd; color: #222222; font-size: 15px;\" />\r\n<p dir=\"ltr\" style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px; text-align: justify;\">TẦM NH&Igrave;N</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Trở th&agrave;nh c&ocirc;ng ty</span>&nbsp;h&agrave;ng đầu Việt Nam&nbsp;<span style=\"box-sizing: border-box;\">trong c&aacute;c lĩnh vực tư vấn thiết kế PCCC-Cơ điện.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">SỨ MỆNH</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Khẳng định chất lượng sản phẩm, dịch vụ, mang tới đối t&aacute;c những trải nghiệm chuy&ecirc;n nghiệp, văn minh v&agrave; đẳng cấp.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">GI&Aacute; TRỊ CỐT L&Otilde;I</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Chất lượng, tiến độ l&agrave; trọng t&acirc;m</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\">TRIẾT L&Yacute; KINH DOANH</p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">Gi&aacute; trị của TDS lu&ocirc;n đi k&egrave;m những cam kết về sự chuy&ecirc;n nghiệp v&agrave; tinh thần tr&aacute;ch nhiệm, đề cao uy t&iacute;n v&agrave; danh dự trong việc n&acirc;ng cao chất lượng dịch vụ đến </span><span style=\"box-sizing: border-box;\">đối t&aacute;c</span><strong style=\"box-sizing: border-box;\">.</strong></p>\r\n<hr style=\"box-sizing: content-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; height: 0px; margin-top: 21px; margin-bottom: 21px; border-right: 0px; border-bottom: 0px; border-left: 0px; border-image: initial; border-top-style: solid; border-top-color: #dddddd; color: #222222; font-size: 15px;\" />\r\n<ul style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-top: 0px; margin-bottom: 20px; line-height: 1.4; color: #646464; clear: both; padding-left: 25px; font-size: 15px;\">\r\n<li style=\"box-sizing: border-box; font-weight: inherit; list-style: square;\">&nbsp;</li>\r\n</ul>', NULL, 'gioi-thieu-chung', NULL, NULL, 'ACTIVE', '2021-01-01 00:59:52', '2021-03-25 15:38:11'),
(3, 1, 'Lịch sử hình thành', NULL, '<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><a class=\"img\" style=\"box-sizing: border-box; background-color: transparent; color: #666666; text-decoration-line: none; outline: none;\" title=\"\" href=\"https://tuvandongduong.vn/upload/ICC%209%20n%C4%83m/vp2.jpg\" data-gallery=\"\"><img style=\"box-sizing: border-box; border: none; vertical-align: middle; max-width: 100%; height: auto; width: 919.986px; display: inline-block; margin-top: 5px; margin-bottom: 5px;\" src=\"https://tuvandongduong.vn/upload/ICC%209%20n%C4%83m/vp2.jpg\" alt=\"\" width=\"1200\" height=\"902\" /></a></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><strong style=\"box-sizing: border-box;\">LỊCH SỬ PH&Aacute;T TRIỂN</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">C&ocirc;ng ty Cổ phần tư vấn thiết kế T Design được th&agrave;nh lập v&agrave;o năm 2011. Đến nay, T Design đ&atilde; trở th&agrave;nh một doanh nghiệp lớn mạnh với hơn 40 nh&acirc;n&nbsp; vi&ecirc;n.</span></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><strong style=\"box-sizing: border-box;\">TDS L&Agrave;M G&Igrave;?</strong></p>\r\n<p style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin: 0px 0px 20px; line-height: 1.4; color: #646464; clear: both; font-size: 15px;\"><span style=\"box-sizing: border-box;\">TDS cung cấp đầy đủ c&aacute;c dịch vụ của C&ocirc;ng ty tư vấn thiết kế hệ thống cơ điện, ph&ograve;ng ch&aacute;y chữa ch&aacute;y bao gồm thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y, thiết kế điện, cấp tho&aacute;t nước, điều h&ograve;a kh&ocirc;ng kh&iacute;, điện nhẹ</span><span style=\"box-sizing: border-box;\">. Ch&uacute;ng t&ocirc;i lu&ocirc;n tự h&agrave;o khi c&oacute; nhiều năm kinh nghiệm tham gia c&aacute;c dự &aacute;n lớn v&agrave; c&oacute; c&ocirc;ng năng phức tạp như&nbsp;</span><span style=\"box-sizing: border-box;\">t&ograve;a nh&agrave; THC Tower, Tropical village B&igrave;nh Minh &ndash; Hội An, Nh&agrave; xưởng sản xuất phụ t&ugrave;ng, sửa chữa, bảo dưỡng &ocirc; t&ocirc; Gia L&acirc;m , Nh&agrave; m&aacute;y phở tại KCN Ti&ecirc;n Sơn, Bắc Ninh, C.ty CP y khoa Th&aacute;i Thịnh, CT TNHH DLND LBNĐ Hội An </span><span style=\"box-sizing: border-box;\">&nbsp;&hellip; c&ugrave;ng với c&aacute;c chủ đầu tư uy t&iacute;n, c&oacute; y&ecirc;u cầu cao về chất lượng sản phẩm</span><span style=\"box-sizing: border-box;\">. Ch&uacute;ng t&ocirc;i tin tưởng rằng với nỗ lực v&agrave; sự tin tưởng của qu&yacute; kh&aacute;ch h&agrave;ng, C&ocirc;ng ty sẽ lu&ocirc;n khẳng định được uy t&iacute;n v&agrave; vững bước đi đến th&agrave;nh c&ocirc;ng. TDS cam kết mang đến qu&yacute; kh&aacute;ch h&agrave;ng dịch vụ tốt nhất ở mỗi dự &aacute;n m&agrave; ch&uacute;ng t&ocirc;i thực hiện.</span></p>\r\n<ul style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-top: 0px; margin-bottom: 20px; line-height: 1.4; color: #646464; clear: both; padding-left: 25px; font-size: 15px;\">\r\n<li style=\"box-sizing: border-box; font-weight: inherit; list-style: square;\"><strong style=\"box-sizing: border-box;\">Tư vấn thiết kế ph&ograve;ng ch&aacute;y chữa ch&aacute;y</strong></li>\r\n</ul>\r\n<ul style=\"box-sizing: border-box; font-family: Montserrat, \'Helvetica Neue\', sans-serif; margin-top: 0px; margin-bottom: 20px; line-height: 1.4; color: #646464; clear: both; padding-left: 25px; font-size: 15px;\">\r\n<li style=\"box-sizing: border-box; font-weight: inherit; list-style: square;\"><strong style=\"box-sizing: border-box;\">Tư vấn thiết kế cơ điện</strong></li>\r\n</ul>', NULL, 'lich-su-hinh-thanh', NULL, NULL, 'ACTIVE', '2021-01-01 02:30:12', '2021-03-25 15:33:58');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(2, 'browse_bread', NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(3, 'browse_database', NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(4, 'browse_media', NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(5, 'browse_compass', NULL, '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(6, 'browse_menus', 'menus', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(7, 'read_menus', 'menus', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(8, 'edit_menus', 'menus', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(9, 'add_menus', 'menus', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(10, 'delete_menus', 'menus', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(11, 'browse_roles', 'roles', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(12, 'read_roles', 'roles', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(13, 'edit_roles', 'roles', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(14, 'add_roles', 'roles', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(15, 'delete_roles', 'roles', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(16, 'browse_users', 'users', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(17, 'read_users', 'users', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(18, 'edit_users', 'users', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(19, 'add_users', 'users', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(20, 'delete_users', 'users', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(21, 'browse_settings', 'settings', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(22, 'read_settings', 'settings', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(23, 'edit_settings', 'settings', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(24, 'add_settings', 'settings', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(25, 'delete_settings', 'settings', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(26, 'browse_categories', 'categories', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(27, 'read_categories', 'categories', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(28, 'edit_categories', 'categories', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(29, 'add_categories', 'categories', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(30, 'delete_categories', 'categories', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(31, 'browse_posts', 'posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(32, 'read_posts', 'posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(33, 'edit_posts', 'posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(34, 'add_posts', 'posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(35, 'delete_posts', 'posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(36, 'browse_pages', 'pages', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(37, 'read_pages', 'pages', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(38, 'edit_pages', 'pages', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(39, 'add_pages', 'pages', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(40, 'delete_pages', 'pages', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(41, 'browse_hooks', NULL, '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(42, 'browse_product', 'product', '2020-12-22 03:28:23', '2020-12-22 03:28:23'),
(43, 'read_product', 'product', '2020-12-22 03:28:23', '2020-12-22 03:28:23'),
(44, 'edit_product', 'product', '2020-12-22 03:28:23', '2020-12-22 03:28:23'),
(45, 'add_product', 'product', '2020-12-22 03:28:23', '2020-12-22 03:28:23'),
(46, 'delete_product', 'product', '2020-12-22 03:28:23', '2020-12-22 03:28:23'),
(47, 'browse_type_product', 'type_product', '2020-12-22 03:42:04', '2020-12-22 03:42:04'),
(48, 'read_type_product', 'type_product', '2020-12-22 03:42:04', '2020-12-22 03:42:04'),
(49, 'edit_type_product', 'type_product', '2020-12-22 03:42:04', '2020-12-22 03:42:04'),
(50, 'add_type_product', 'type_product', '2020-12-22 03:42:04', '2020-12-22 03:42:04'),
(51, 'delete_type_product', 'type_product', '2020-12-22 03:42:04', '2020-12-22 03:42:04'),
(52, 'browse_slide', 'slide', '2021-01-01 10:33:58', '2021-01-01 10:33:58'),
(53, 'read_slide', 'slide', '2021-01-01 10:33:58', '2021-01-01 10:33:58'),
(54, 'edit_slide', 'slide', '2021-01-01 10:33:58', '2021-01-01 10:33:58'),
(55, 'add_slide', 'slide', '2021-01-01 10:33:58', '2021-01-01 10:33:58'),
(56, 'delete_slide', 'slide', '2021-01-01 10:33:58', '2021-01-01 10:33:58'),
(62, 'browse_configs', 'configs', '2021-02-05 06:25:18', '2021-02-05 06:25:18'),
(63, 'read_configs', 'configs', '2021-02-05 06:25:18', '2021-02-05 06:25:18'),
(64, 'edit_configs', 'configs', '2021-02-05 06:25:18', '2021-02-05 06:25:18'),
(65, 'add_configs', 'configs', '2021-02-05 06:25:18', '2021-02-05 06:25:18'),
(66, 'delete_configs', 'configs', '2021-02-05 06:25:18', '2021-02-05 06:25:18');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(42, 1),
(42, 3),
(43, 1),
(43, 3),
(44, 1),
(44, 3),
(45, 1),
(45, 3),
(46, 1),
(46, 3),
(47, 1),
(47, 3),
(48, 1),
(48, 3),
(49, 1),
(49, 3),
(50, 1),
(50, 3),
(51, 1),
(51, 3),
(52, 1),
(52, 3),
(53, 1),
(53, 3),
(54, 1),
(54, 3),
(55, 1),
(55, 3),
(56, 1),
(56, 3),
(62, 1),
(62, 3),
(63, 1),
(63, 3),
(64, 1),
(64, 3),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) NOT NULL COMMENT 'id loại sản phẩm',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'products/product_default.png' COMMENT 'list ảnh: mảng hoặc 1 ảnh',
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Administrator', '2020-12-21 02:36:01', '2020-12-22 18:48:57'),
(2, 'user', 'Normal User', '2020-12-21 02:36:01', '2020-12-21 02:36:01'),
(3, 'admin', 'Admin', '2020-12-22 18:49:55', '2020-12-22 18:49:55');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings/December2020/J55yuDZjVnq9fZFdkGi6.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'T-Design', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Công ty cổ phần tư vấn thiết kế T-Design', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/February2021/2H6MipOWp1QveK0uP84t.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slide`
--

CREATE TABLE `slide` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'đường dẫn liên kết cho ảnh của slide',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slide`
--

INSERT INTO `slide` (`id`, `title`, `image`, `link`, `created_at`, `updated_at`) VALUES
(1, 'DỰ ÁN|VINHOMES CENTER PARK', 'slide/January2021/g7mEZxu4UQO7DZCWaVUS.jpg', NULL, '2021-01-01 10:39:00', '2021-04-08 10:07:21'),
(2, 'NỘI THẤT PHÒNG KHÁCH|BIỆT THỰ VILLA MR. HOANG', 'slide/January2021/n5UH5MOcGAeuWgj13HcR.jpg', NULL, '2021-01-01 10:46:00', '2021-01-01 11:43:38'),
(3, 'DỰ ÁN|VINPEARL ĐẦM GIÀ - HÒN TRE', 'slide/January2021/EsuOdIKK23AWFz1knZgR.jpg', NULL, '2021-01-01 10:46:00', '2021-01-01 11:43:17'),
(4, 'NỘI THẤT PHÒNG ĂN|BIỆT THỰ ANH HOÀNG', 'slide/January2021/7L6NOe9461irmCMawpww.jpg', NULL, '2021-01-01 10:47:00', '2021-01-01 11:43:08'),
(5, 'NỘI THẤT PHÒNG NGỦ|CHUNG CƯ  CHỊ LINH - OCEAN PARK', 'slide/January2021/YUUkN1aV9HAuKnSXx04C.jpg', NULL, '2021-01-01 10:48:00', '2021-01-01 11:42:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2020-12-21 02:36:02', '2020-12-21 02:36:02'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2020-12-21 02:36:02', '2020-12-21 02:36:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `type_product`
--

CREATE TABLE `type_product` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `type_product`
--

INSERT INTO `type_product` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Hỗn hợp công trình', 'hon-hop-cong-trinh', '2020-12-22 03:44:00', '2020-12-22 21:22:06'),
(2, 'Văn Phòng', 'van-phong', '2020-12-22 21:22:28', '2020-12-22 21:22:28'),
(3, 'Bệnh viện', 'benh-vien', '2020-12-22 21:22:35', '2020-12-22 21:22:35'),
(4, 'Trường học', 'truong-hoc', '2020-12-22 21:22:50', '2020-12-22 21:22:50'),
(5, 'Trung tâm thương mại', 'trung-tam-thuong-mai', '2020-12-23 03:35:36', '2020-12-23 03:35:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Super Admin', 'superadmin@admin.com', 'users/December2020/7yu1TEBdy29vB9SUfXiW.png', NULL, '$2y$10$sK711E/aWpoRaZRQn8/N..sVxZLrxzdmNmIPcqWFgHXfa97ongOaq', 'dM6EkZlzxXHHhWyh92HLydKe5cYligJV4ILiMeRRLTdTOz4CEH2Tyu9WY0nO', '{\"locale\":\"vi\"}', '2020-12-21 02:36:01', '2020-12-30 20:47:02'),
(2, 3, 'Admin', 'admin@tdesign.com', 'users/February2021/dCjeH9e15neo8r4Ytny3.jpg', NULL, '$2y$10$YTo9rAf2gF7DPBrQbfACReNNlCbberICjLNUwHIcYGzEJ3f7YAzVO', 'rbCWurVxCaI51hAslFVZcNxkPXNFGfsVVCTA3CK2pYMJqObh1YYkuK8pPGUw', '{\"locale\":\"vi\"}', '2020-12-22 18:50:24', '2021-02-05 06:35:25');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Chỉ mục cho bảng `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Chỉ mục cho bảng `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Chỉ mục cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Chỉ mục cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_slug_unique` (`slug`),
  ADD KEY `product_slug_created_at_index` (`slug`,`created_at`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Chỉ mục cho bảng `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Chỉ mục cho bảng `type_product`
--
ALTER TABLE `type_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type_product_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Chỉ mục cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `configs`
--
ALTER TABLE `configs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT cho bảng `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `slide`
--
ALTER TABLE `slide`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT cho bảng `type_product`
--
ALTER TABLE `type_product`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Các ràng buộc cho bảng `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Các ràng buộc cho bảng `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
